package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.Post;
import model.User;
/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {
	 public String cript(String pass) throws NoSuchAlgorithmException {
	//ハッシュを生成したい元の文字列
	String source = pass;
	//ハッシュ生成前にバイト配列に置き換える際のCharset
	Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
	String algorithm = "MD5";

	//ハッシュ生成処理
	byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	String result = DatatypeConverter.printHexBinary(bytes);

	return result;
	 }

/**
 * ログインIDとパスワードに紐づくユーザ情報を返す
 * @param logID
 * @param logPass
 * @return
 * @throws NoSuchAlgorithmException
 */
public User findByLoginInfo(String logID, String logPass) throws NoSuchAlgorithmException  {

    Connection conn = null;
    try {

    	String pass=cript(logPass);
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

         // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, logID);
        pStmt.setString(2, pass);
        ResultSet rs = pStmt.executeQuery();


        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        if (!rs.next()) {
            return null;
        }

        // 必要なデータのみインスタンスのフィールドに追加
        int userId = rs.getInt("user_id");
        String nameData = rs.getString("user_name");
        String addressData = rs.getString("user_address");
        String loginIdData = rs.getString("login_id");
        String passData = rs.getString("password");
        Date createDate = rs.getDate("create_date");

        return new User(userId,nameData,addressData,loginIdData, passData,createDate);

    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
	return null;
}
//idに基づくユーザー情報を取得//

public model.User findById(String userId) {
    Connection conn = null;
    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM user WHERE user_id =?";

         // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, userId);
        ResultSet rs = pStmt.executeQuery();


        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        if (!rs.next()) {
            return null;
        }

        // 必要なデータのみインスタンスのフィールドに追加
        int Id = rs.getInt("user_id");
        String userName = rs.getString("user_name");
        String userAddress = rs.getString("user_address");
        String loginId = rs.getString("login_id");
        String password = rs.getString("password");
        Date createData = rs.getDate("create_date");
        User user= new User(Id, userName, userAddress,loginId,password,createData);


        return user;

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
//ログインID
public User findByLoginId(String loginId) {
    Connection conn = null;
    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM user WHERE login_id =?";

         // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, loginId);
        ResultSet rs = pStmt.executeQuery();


        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        if (!rs.next()) {
            return null;
        }

        // 必要なデータのみインスタンスのフィールドに追加
        int userId = rs.getInt("user_id");
        String userName = rs.getString("user_name");
        String userAddress = rs.getString("user_address");
        String loginIdData = rs.getString("login_id");
        String pass = rs.getString("password");

        User user = new User(userId,userName,userAddress,loginIdData,pass);

        return user;

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
public String findByLoginId2(String loginId) {
    Connection conn = null;
    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM user WHERE login_id =?";

         // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, loginId);
        ResultSet rs = pStmt.executeQuery();


        // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
        if (!rs.next()) {
            return null;
        }

        // 必要なデータのみインスタンスのフィールドに追加

        String loginIdData = rs.getString("login_id");


        return loginIdData;

    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
//お気に入りselect
public List<Post> favoritePost(int id) {
  Connection conn = null;
  List<Post> postList = new ArrayList<Post>();
  try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM favorite join message on message.post_id=favorite.post_id where favorite.`user_id`=? ;";

       // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);

      ResultSet rs = pStmt.executeQuery();


      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      while (rs.next()) {
      	int postId = rs.getInt("post_id");

          String posIm = rs.getString("post_image");
          String posCon = rs.getString("post_contents");

          Post post = new Post(postId,posIm,posCon);

          postList.add(post);
      }
  } catch (SQLException e) {
      e.printStackTrace();
      return null;
  } finally {
      // データベース切断
      if (conn != null) {
          try {
              conn.close();
          } catch (SQLException e) {
              e.printStackTrace();
              return null;
          }
      }
  }
return postList;
}

//ユーザーID
public String findByUserId(String loginId) {
  Connection conn = null;
  try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT user_id FROM user WHERE login_id =?";

       // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();


      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
          return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加

      String userIdDate = rs.getString("user_id");


      return userIdDate;

  } catch (SQLException e) {
      e.printStackTrace();
      return null;
  } finally {
      // データベース切断
      if (conn != null) {
          try {
              conn.close();
          } catch (SQLException e) {
              e.printStackTrace();
              return null;
          }
      }
  }
}

//
public String findByPostId(String contents) {
Connection conn = null;
try {
    // データベースへ接続
    conn = DBManager.getConnection();

    // SELECT文を準備
    String sql = "SELECT post_id FROM user WHERE post_contents =?";

     // SELECTを実行し、結果表を取得
    PreparedStatement pStmt = conn.prepareStatement(sql);
    pStmt.setString(1, contents);
    ResultSet rs = pStmt.executeQuery();


    // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
    if (!rs.next()) {
        return null;
    }

    // 必要なデータのみインスタンスのフィールドに追加

    String postIdData = rs.getString("post_id");


    return postIdData;

} catch (SQLException e) {
    e.printStackTrace();
    return null;
} finally {
    // データベース切断
    if (conn != null) {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
}
//新規登録
public void NewUser(String loginId,String pass,String userName,String userAddress) throws NoSuchAlgorithmException {
	String pass1=cript(pass);
    Connection conn = null;
    try {


        // データベースへ接続
        conn = DBManager.getConnection();

        // Insert文を準備
        String sql = "insert into user (user_name,user_address,login_id,password,create_date)values(?,?,?,?,now());";

         // Insertを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, userName);
        pStmt.setString(2, userAddress);
        pStmt.setString(3, loginId);
        pStmt.setString(4, pass1);

        pStmt.executeUpdate();


    } catch (SQLException e) {
        e.printStackTrace();
    }finally {

        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
//クレジット登録
public void NewCredit(String creditCompany,String creditNum,String creditExDate,String creditName,String userId) throws NoSuchAlgorithmException {

  Connection conn = null;
  try {




      // データベースへ接続
      conn = DBManager.getConnection();

      // Insert文を準備
      String sql = "insert into credit (credit_company,credit_num,credit_exDate,credit_name,user_id)values(?,?,?,?,?)";

       // Insertを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, creditCompany);
      pStmt.setString(2, creditNum);
      pStmt.setString(3, creditExDate);
      pStmt.setString(4, creditName);
      pStmt.setString(5, userId);

      pStmt.executeUpdate();


  } catch (SQLException e) {
      e.printStackTrace();
  }finally {

      // データベース切断
      if (conn != null) {
          try {
              conn.close();
          } catch (SQLException e) {
              e.printStackTrace();
          }
      }
  }
}
//情報更新
public void UpdateUser(String loginId,String userName,String userAddress)  {

    Connection conn = null;
    try {

        // データベースへ接続
        conn = DBManager.getConnection();

        // Update文を準備
        String sql = "Update user Set user_name=?,user_address=? where login_id=?;";

         // Updateを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, userName);
        pStmt.setString(2, userAddress);

        pStmt.setString(3, loginId);

        pStmt.executeUpdate();


    } catch (SQLException e) {
        e.printStackTrace();
    }
      finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }






}

public void NewPost(String photo, String contents,String userIdDate) {
	 Connection conn = null;
	    try {


	        // データベースへ接続
	        conn = DBManager.getConnection();

	        // Insert文を準備
	        String sql = "insert into message (post_image,post_contents,user_id)values(?,?,?);";

	         // Insertを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, photo);
	        pStmt.setString(2, contents);
	        pStmt.setString(3, userIdDate);


	        pStmt.executeUpdate();


	    } catch (SQLException e) {
	        e.printStackTrace();
	    }finally {

	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
//お気に入り
public void FavoritePost(String postData,String userIdDate) {
	 Connection conn = null;
	    try {


	        // データベースへ接続
	        conn = DBManager.getConnection();

	        // Insert文を準備
	        String sql = "insert into favorite (post_id,user_id)values(?,?);";

	         // Insertを実行し、結果表を取得
	        PreparedStatement pStmt = conn.prepareStatement(sql);
	        pStmt.setString(1, postData);

	        pStmt.setString(2, userIdDate);


	        pStmt.executeUpdate();


	    } catch (SQLException e) {
	        e.printStackTrace();
	    }finally {

	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
public List<Post> postAll(String userIdData) {
    Connection conn = null;
    List<Post> postList = new ArrayList<Post>();

    try {
        // データベースへ接続
        conn = DBManager.getConnection();

        // SELECT文を準備
        // TODO: 未実装：管理者以外を取得するようSQLを変更する
        String sql = "SELECT \n" +
        		"m.post_id,\n" +
        		"m.post_contents,\n" +
        		"m.post_image,\n" +
        		"count(f.post_id) favorite_count \n" +
        		"FROM \n" +
        		"message m\n" +
        		"LEFT OUTER JOIN\n" +
        		"(SELECT * FROM favorite f WHERE f.user_id = ?) f\n" +
        		"ON \n" +
        		"m.post_id = f.post_id\n" +
        		"GROUP BY m.post_id;";

         // SELECTを実行し、結果表を取得
        PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, userIdData);
        ResultSet rs = pStmt.executeQuery();

        // 結果表に格納されたレコードの内容を
        // Userインスタンスに設定し、ArrayListインスタンスに追加
        while (rs.next()) {
        	int postId = rs.getInt("post_id");
            String posCon = rs.getString("post_contents");
            String posIm = rs.getString("post_image");
            int count=rs.getInt("favorite_count");

            Post post = new Post(postId,posIm,posCon,count);

            postList.add(post);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return postList;
}

//ユーザー削除
public void DeleteUser(String userId) {
	 Connection conn = null;
     try {
         // データベースへ接続
         conn = DBManager.getConnection();

         // Delete文を準備
         String sql = "Delete From user Where user_id=?";

          // Deleteを実行し、結果表を取得
         PreparedStatement pStmt = conn.prepareStatement(sql);
         pStmt.setString(1, userId);

         pStmt.executeUpdate();


     } catch (SQLException e) {
         e.printStackTrace();
     } finally {
         // データベース切断
         if (conn != null) {
             try {
                 conn.close();
             } catch (SQLException e) {
                 e.printStackTrace();
             }
         }
     }
 }

public String findByCreditNum(String creditNum) {
	Connection conn = null;
	try {
	    // データベースへ接続
	    conn = DBManager.getConnection();

	    // SELECT文を準備
	    String sql = "SELECT * FROM credit WHERE  credit_num=?";

	     // SELECTを実行し、結果表を取得
	    PreparedStatement pStmt = conn.prepareStatement(sql);
	    pStmt.setString(1, creditNum);
	    ResultSet rs = pStmt.executeQuery();


	    // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	    if (!rs.next()) {
	        return null;
	    }

	    // 必要なデータのみインスタンスのフィールドに追加

	    String creditNum1 = rs.getString("credit_num");


	    return creditNum1;

	} catch (SQLException e) {
	    e.printStackTrace();
	    return null;
	} finally {
	    // データベース切断
	    if (conn != null) {
	        try {
	            conn.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }
	    }
	}


}


}