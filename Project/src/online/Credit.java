package online;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.CreditM;
import model.User;

/**
 * Servlet implementation class Credit
 */
@WebServlet("/Credit")
public class Credit extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @param creditName
     * @param creditExDate
     * @param creditNum
     * @param creditCompany
     * @see HttpServlet#HttpServlet()
     */



	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/credit.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			// リクエストパラメータの文字コードを指定
	        request.setCharacterEncoding("UTF-8");

			String creditCompany = request.getParameter("credit_company");
			String creditNum = request.getParameter("credit_num");
			String creditExDate = request.getParameter("credit_exDate");
			String creditName = request.getParameter("credit_name");


			UserDao userDao = new UserDao();
			String creditNum2=userDao.findByCreditNum(creditNum);

			if(creditCompany.equals("")||creditNum.equals("")||creditExDate.equals("")||creditName.equals("")||creditNum.equals(creditNum2)) {
				request.setAttribute("checkErr", "入力した内容は正しくありません");
				// ログインjspにフォワード
							RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/credit.jsp");
							dispatcher.forward(request, response);
							return;
			}
			HttpSession session = request.getSession();
				User user=(User) session.getAttribute("userInfo");

			    String userIdDate=userDao.findByUserId(user.getLoginId());



			//NewUser
				userDao.NewCredit(creditCompany,creditNum,creditExDate,creditName,userIdDate);



				CreditM credit =new CreditM(creditCompany,creditNum,creditExDate,creditName,userIdDate);
				// セッションにユーザの情報をセット
				session.setAttribute("creditInfo", credit);




				// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/creditR.jsp");
				dispatcher.forward(request, response);



			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
		}


	}
}


