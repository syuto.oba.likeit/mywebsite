package online;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class UserC
 */
@WebServlet("/UserC")
public class UserC extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("login_id");
		String userName = request.getParameter("user_name");
		String userAddress = request.getParameter("user_address");

		UserDao userDao = new UserDao();
		if(userName.equals("")||loginId.equals("")||userAddress.equals("")) {
			request.setAttribute("passErr","入力された内容は正しくありません。");
			// ユーザ情報をリクエストスコープにセットしてjspにフォワード
			request.setAttribute("loginId",loginId);
			request.setAttribute("userName",userName);
			request.setAttribute("userAddress",userAddress);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userC.jsp");
			dispatcher.forward(request, response);
			return ;
		}



		//UpdateUser
		userDao.UpdateUser(loginId,userName,userAddress);

		request.setAttribute("loginId",loginId);
		request.setAttribute("userName",userName);
		request.setAttribute("userAddress",userAddress);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userR.jsp");
		dispatcher.forward(request, response);
		return;

	}

}
