package online;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Regist
 */
@WebServlet("/Regist")
public class Regist extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Regist() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
				dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("login_id");
		String pass = request.getParameter("password");
		String passC = request.getParameter("confirm_password");
		String userName = request.getParameter("user_name");
		String userAddress = request.getParameter("user_address");


		UserDao userDao = new UserDao();
		String loginId2 =userDao.findByLoginId2(loginId);

		if((!(pass.equals(passC)))||loginId.equals("")||pass.equals("")||passC.equals("")||userName.equals("")||userAddress.equals("")||loginId.equals(loginId2)) {
			request.setAttribute("checkErr", "入力した内容は正しくありません");
			// ログインjspにフォワード
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/regist.jsp");
						dispatcher.forward(request, response);
						return;
		}

		//NewUser

			userDao.NewUser(loginId,pass,userName,userAddress);

			User user=userDao.findByLoginId(loginId);


			request.setAttribute("user", user);

			// セッションにユーザの情報をセット
			HttpSession session = request.getSession();
			session.setAttribute("userInfo", user);


			// フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registR.jsp");
			dispatcher.forward(request, response);



		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}
}


