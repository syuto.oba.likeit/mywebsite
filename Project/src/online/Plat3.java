package online;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.Post;
import model.User;

/**
 * Servlet implementation class Plat3
 */
@WebServlet("/Plat3")
public class Plat3 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Plat3() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDao userDao = new UserDao();
		HttpSession session = request.getSession();
		User user=(User) session.getAttribute("userInfo");
	    String userIdDate=userDao.findByUserId(user.getLoginId());
		List<Post> postList=userDao.postAll(userIdDate);
		request.setAttribute("postList", postList);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/plat3.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserDao userDao = new UserDao();
//		ユーザーID取得
		HttpSession session = request.getSession();
		User user=(User) session.getAttribute("userInfo");
	    String userIdDate=userDao.findByUserId(user.getLoginId());
//	    ポストIDの取得
	    String postIdData = request.getParameter("postId");

	    userDao.FavoritePost(postIdData,userIdDate);

		List<Post> postList=userDao.postAll(userIdDate);
		request.setAttribute("postList", postList);
	 // フォワード
	 		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/plat3.jsp");
	 		dispatcher.forward(request, response);






	}

}
