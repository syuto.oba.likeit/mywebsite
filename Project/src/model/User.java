package model;

import java.io.Serializable;
import java.util.Date;

/**
 * Userテーブルのデータを格納するためのBeans
 * @author takano
 *
 */
public class User implements Serializable {
	private int userId;
	private String userName;
	private String userAddress;
	private String loginId;
	private String password;

	private Date createDate;

	public User(int userId, String userName, String userAddress, String loginId, String password,
			Date createDate) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userAddress = userAddress;
		this.loginId = loginId;
		this.password = password;

		this.createDate = createDate;
	}

	// ログインセッションを保存するためのコンストラクタ

	public User(String userName, String userAddress, String loginId, String password, Date createDate) {
		this.userName = userName;
		this.userAddress = userAddress;
		this.loginId = loginId;
		this.password = password;
		this.createDate = createDate;
	}


	public User(int userId,String userName, String userAddress, String loginId, String password) {
		this.userId=userId;
		this.userName = userName;
		this.userAddress = userAddress;
		this.loginId = loginId;
		this.password = password;

	}



	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUserName() {
		return userName;
	}
	public void setName(String userName) {
		this.userName = userName;
	}
	public String getUserAddress() {
		return userAddress;
	}
	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


}
