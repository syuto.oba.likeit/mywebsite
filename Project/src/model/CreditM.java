package model;

import java.io.Serializable;

import javax.servlet.annotation.WebServlet;

/**
 * Servlet implementation class CreditM
 */
@WebServlet("/CreditM")
public class CreditM implements Serializable {
	private String creditCompany;
	private String creditNum;
	private String creditExDate;
	private String creditName;
	private String userId;

	public CreditM(String creditCompany, String creditNum, String creditExDate, String creditName,String userId) {
		super();
		this.creditCompany = creditCompany;
		this.creditNum = creditNum;
		this.creditExDate = creditExDate;
		this.creditName = creditName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCreditCompany() {
		return creditCompany;
	}
	public void setCreditCompany(String creditCompany) {
		this.creditCompany = creditCompany;
	}
	public String getCreditNum() {
		return creditNum;
	}
	public void setCreditNum(String creditNum) {
		this.creditNum = creditNum;
	}
	public String getCreditExDate() {
		return creditExDate;
	}
	public void setCreditExDate(String creditExDate) {
		this.creditExDate = creditExDate;
	}
	public String getCreditName() {
		return creditName;
	}
	public void setCreditName(String creditName) {
		this.creditName = creditName;
	}



}
