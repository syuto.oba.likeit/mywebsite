package model;

import java.io.Serializable;

/**
 * Postテーブルのデータを格納するためのBeans
 * @author takano
 *
 */
public class Post implements Serializable {
	private int postId;
	private String postPhoto;
	private String postContents;
	private int count;



	public Post(int postId, String postPhoto, String postContents, int count) {
		this.postId = postId;
		this.postPhoto = postPhoto;
		this.postContents = postContents;
		this.count = count;


	}
	public Post(int postId, String postPhoto, String postContents) {
		super();
		this.postId = postId;
		this.postPhoto = postPhoto;
		this.postContents = postContents;
	}
	public int getPostId() {
		return postId;
	}
	public void setPostId(int postId) {
		this.postId = postId;
	}
	public String getPostPhoto() {
		return postPhoto;
	}
	public void setPostPhoto(String postPhoto) {
		this.postPhoto = postPhoto;
	}

	public String getPostContents() {
		return postContents;
	}
	public void setPostContents(String postContents) {
		this.postContents = postContents;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}



}