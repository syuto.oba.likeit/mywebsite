<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>クレジット</title>
	<link rel="stylesheet" href="css/style1.css"/>
     <link rel="stylesheet" href="css/style.css"/>
</head>

        <header class="header">
            <h1 class="logo">
                <a href="Plat">
                 Ring.jp
                </a>
            </h1>


        <nav class="global-nav">
          <ul class="global-nav__list">
            <li class="global-nav__item">
              <a href="Login">ログイン</a>
            </li>
           </ul>
          </nav>

        </header>
<body>
	<br>
	<br>
	<div class="container">
	<c:if test="${checkErr != null}">
	    <div class="alert alert-danger" role="alert">
		  ${checkErr}
		</div>
</c:if>
		<div class="row center">
			<h5 class=" col s12 light">クレジット</h5>

				<P class="red-text"></P>

		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s6 offset-s3">
				<div class="card grey lighten-5">
					<div class="card-content">
						<form action="Credit" method="POST">
							<div class="row">
								<div class="input-field col s10 offset-s1">
                                    <label>クレジット会社</label>
									<input value="" name="credit_company" type="text" required>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
                                    <label>クレジット番号</label>
									<input value="" name="credit_num" type="text" required>
								</div>
							</div>


							<div class="row">
								<div class="input-field col s10 offset-s1">
                                    <label>クレジット有効期限</label>
									<input value="" name="credit_exDate" type="text" required>
								</div>
							</div>

							<div class="row">
								<div class="input-field col s10 offset-s1">
                                    <label>クレジット名前</label>
									<input name="credit_name" type="text" required>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<p class="center-align">
										<button class="btn btn-large waves-effect waves-light  col s8 offset-s2" type="submit" name="action">完了</button>
									</p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>