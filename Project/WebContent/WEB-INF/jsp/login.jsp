<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8"/>
    <title>Ring.jp</title>
    <link rel="stylesheet" href="css/style1.css"/>
    <link rel="stylesheet" href="css/style.css"/>

</head>
<body>

<c:if test="${errMsg != null}">
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
</c:if>

        <header class="header">
            <h1 class="logo">
                <a href="Plat">
                 Ring.jp
                </a>
            </h1>


        <nav class="global-nav">
          <ul class="global-nav__list">
            <li class="global-nav__item">
              <a href="Login">ログイン</a>
            </li>
           </ul>
          </nav>

        </header>



        <main class="main">
      <div class="main__wrap">
                <div class="main__wrap__content">

<article class="login">

    <div class="container__headline">
    <section class="headline">
      <div class="headline__icon">
        <a class="link--nostyle" href="/">
          <img src="https://scontent-nrt1-1.xx.fbcdn.net/v/t1.0-9/1471088_803382306355162_1075974222_n.jpg?_nc_cat=105&_nc_sid=85a577&_nc_ohc=LnkdWAiSJxUAX8Lmmwb&_nc_ht=scontent-nrt1-1.xx&oh=2f7df61693cf0ea57406b3e3bca7c5e8&oe=5F4BCFCB" alt="Ring">
        </a>
      </div>
      <h1 class="headline__title fs-20">
        <span class="headline__title__text emphasis">ログイン</span>
      </h1>
    </section>
  </div>

<form action="Login" method="Post">
  <div class="container__login__actions">
    <section class="login__actions mb-10">
      <p class="login">
			ログインID <input class="ml-3" type="text" name="logID" style="width: auto">
		</p>

		<p class="login">
			パスワード <br>
			<input class="ml-3" type="password" name="logPass" style="width: auto;">
		</p>

        <input type="submit"  class="btn btn-outline-primary" value="ログイン"   >
    </section>
  </div>
 </form>

</article>

<script>
  $(function() {
    setTimeout(function() {
      FB.getLoginStatus(function(response) {
        if (response.status !== 'unknown') {
          $('.js-fb-login-status').show();
        }
      }, true);
    }, 1500);
  });
</script>
        </div>
      </div>
    </main>

          <footer class="footer">
        <div class="footer__wrap">
          <nav class="footer__wrap__nav">
            <ul class="footer__wrap__nav__list">
                            <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle emphasis" href="Plat">
                  <i class="icon icon-mr-10 icon--home"></i>
                  トップに戻る                </a>
              </li>
                                          <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle emphasis" href="Login">
                  ログイン                </a>
              </li>
                          </ul>
          </nav>
          <nav class="footer__wrap__nav">
            <ul class="footer__wrap__nav__list">
              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="TermsService">
                  利用規約
                </a>
              </li>
              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="Privacy">
                  プライバシーポリシー                </a>
              </li>
              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="TransactLaw">
                  特定商取引法に基づく表記                </a>
              </li>
            </ul>
          </nav>
          <nav class="footer__wrap__nav">
            <ul class="footer__wrap__nav__list">
              <li class="footer__wrap__nav__list__item">
                              <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="Form" target="_blank">
                  お問い合わせ                              </a>
              </li>
                          </ul>
          </nav>
          <div class="footer__wrap__copylight">
            <div class="footer__wrap__copylight__wrap">
              <span class="footer__wrap__copylight__wrap__text">© 株式会社Ring</span>
            </div>
          </div>
        </div>
      </footer>



<ins class="adsbygoogle adsbygoogle-noablate" data-adsbygoogle-status="done" style="display: none !important;"><ins id="aswift_0_expand" style="display:inline-table;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:0px;background-color:transparent;"><ins id="aswift_0_anchor" style="display:block;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:0px;background-color:transparent;"><iframe id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;border:0;width:undefinedpx;height:undefinedpx;" sandbox="allow-forms allow-pointer-lock allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" frameborder="0" src="https://googleads.g.doubleclick.net/pagead/ads?client=ca-pub-9476476657694318&amp;output=html&amp;adk=1812271804&amp;adf=3025194257&amp;lmt=1596113289&amp;plat=1%3A32776%2C2%3A16809992%2C9%3A32776%2C10%3A32%2C11%3A32%2C16%3A8388608%2C17%3A32%2C24%3A32%2C25%3A32%2C30%3A1048576%2C32%3A32%2C40%3A32&amp;guci=2.2.0.0.2.2.0.0&amp;format=0x0&amp;url=https%3A%2F%2Fsalon.jp%2Fauthentications%2Flogin&amp;ea=0&amp;flash=0&amp;pra=5&amp;wgl=1&amp;dt=1596113289762&amp;bpp=3&amp;bdt=112&amp;idt=14&amp;shv=r20200727&amp;cbv=r20190131&amp;ptt=9&amp;saldr=aa&amp;abxe=1&amp;nras=1&amp;correlator=7007587157243&amp;frm=20&amp;pv=2&amp;ga_vid=1567067410.1596083507&amp;ga_sid=1596113290&amp;ga_hid=488121684&amp;ga_fc=1&amp;iag=0&amp;icsg=2147745440&amp;dssz=22&amp;mdo=0&amp;mso=0&amp;u_tz=540&amp;u_his=6&amp;u_java=0&amp;u_h=900&amp;u_w=1440&amp;u_ah=836&amp;u_aw=1440&amp;u_cd=30&amp;u_nplug=3&amp;u_nmime=4&amp;adx=-12245933&amp;ady=-12245933&amp;biw=1029&amp;bih=720&amp;scr_x=0&amp;scr_y=0&amp;oid=3&amp;pvsid=2953345718301277&amp;pem=718&amp;ref=https%3A%2F%2Fsalon.jp%2Fsalons%2Fview&amp;rx=0&amp;eae=2&amp;fc=1920&amp;brdim=398%2C131%2C398%2C131%2C1440%2C23%2C1029%2C831%2C1029%2C720&amp;vis=1&amp;rsz=%7C%7Cs%7C&amp;abl=NS&amp;fu=8192&amp;bc=31&amp;ifi=0&amp;uci=a!0&amp;fsb=1&amp;dtd=23" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" data-google-container-id="a!0" data-load-complete="true"></iframe></ins></ins></ins><iframe id="google_osd_static_frame_4921095719280" name="google_osd_static_frame" style="display: none; width: 0px; height: 0px;"></iframe></body>