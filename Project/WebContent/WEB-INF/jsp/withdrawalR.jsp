<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <link rel ="stylesheet" href="css/style1.css">
    <link rel ="stylesheet" href="css/style.css"></head>

<body>
   <header class="header">
    <div class="block"><h1 class="logo">
				<a href="Plat3"> Ring.jp </a>
			</h1></div>
    <div class="block"><a href="UserN">${userInfo.userName}</a></div>
    <div class="block"><ul class="example">
    	<li><a href="Favorite">お気に入り</a></li>
       <c:if test="${userInfo.loginId =='admin'}">
        <li><a href="Post">投稿</a></li>
        </c:if>
        <li><a href="Credit">クレジット変更</a></li>
        <li><a href="Logout">ログアウト</a></li></ul></div>
    </header>
	<p>e</p>
	<h1 class="delete" accesskey=""align="center">退会完了</h1>

	<p align="center">ユーザー名<br>
	退会処理が完了しました。
	</p>
<form action="Plat3" method="get">
<div class="col-2 mx-auto" align="center">
<input class="returnB"  type="submit" value="マイページに戻る">
</div>
</form>


   <footer class="footer">
        <div class="footer__wrap">
          <nav class="footer__wrap__nav">
            <ul class="footer__wrap__nav__list">
                            <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle emphasis" href="Plat">
                  <i class="icon icon-mr-10 icon--home"></i>
                  トップに戻る                </a>
              </li>
                                          <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle emphasis" href="Login">
                  ログイン                </a>
              </li>
                          </ul>
          </nav>
          <nav class="footer__wrap__nav">
            <ul class="footer__wrap__nav__list">
              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="TermService">
                  利用規約
                </a>
              </li>
              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="Privacy">
                  プライバシーポリシー                </a>
              </li>
              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="TransactLaw">
                  特定商取引法に基づく表記                </a>
              </li>
            </ul>
          </nav>
          <nav class="footer__wrap__nav">
            <ul class="footer__wrap__nav__list">
              <li class="footer__wrap__nav__list__item">
                              <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="Form" target="_blank">
                  お問い合わせ                              </a>
              </li>
                              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="Withdrawal">退会する</a>
                </li>
                          </ul>
          </nav>
          <div class="footer__wrap__copylight">
            <div class="footer__wrap__copylight__wrap">
              <span class="footer__wrap__copylight__wrap__text">© ring株式会社</span>
            </div>
          </div>
        </div>
      </footer>


</body>
</html>