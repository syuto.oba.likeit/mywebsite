<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>


<link rel ="stylesheet" href="css/style1.css">
    <link rel="stylesheet" href="css/style.css"/>
</head>


<body>
    <header class="header">
    <div class="block"><h1 class="logo">
				<a href="Plat3"> Ring.jp </a>
			</h1></div>
    <div class="block"><a href="UserN">${userInfo.userName}</a></div>
    <div class="block"><ul class="example">
    	<li><a href="Favorite">お気に入り</a></li>
    	<c:if test="${userInfo.loginId =='admin'}">
        <li><a href="Post">投稿</a></li>
        </c:if>
        <li><a href="Credit">クレジット変更</a></li>
        <li><a href="Logout">ログアウト</a></li></ul></div>
    </header>

	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">入力内容確認</h5>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<form action="UserC" method="Post">
						<c:if test="${validationMessage != null}">
								<p class="red-text center-align">${validationMessage}</p>
							</c:if>
							<br> <br>
							<div class="row">

								<div class="input-field col s6">
                                    <label>ログインID</label>
									<P>${userInfo.loginId}</P>
									<input type="hidden" name="login_id" value="${userInfo.loginId}">
								<div class="input-field col s6">
                                    <label>名前</label>
									<input type="text" name="user_name" value="${userName}">
								</div>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s12">
                                    <label>住所</label>
									<input type="text" name="user_address" value="${userAddress}">
								</div>
							</div>


							<div class="row">
								<div class="col s12">
									<p class="center-align">上記内容で更新してよろしいでしょうか?</p>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<div class="col s6 center-align">
										<button class="btn  waves-effect waves-light  col s6 offset-s3" type="submit" name="confirmButton" value="update">更新</button>
									</div>
								</div>
							</div>
						</form>
							<div class="col s6 center-align">
									<a href="UserN"><button class="btn  waves-effect waves-light  col s6 offset-s3" type="submit" name="confirmButton" value="cancel">戻る</button></a>
									</div>
					</div>
				</div>
			</div>
		</div>


</body>
</html>