<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ-登録完了</title>
<link rel="stylesheet" href="css/style1.css"/>
<link rel="stylesheet" href="css/style.css"/>
</head>

      <header class="header">
            <h1 class="logo">

                 Ring.jp

            </h1>


        <nav class="global-nav">
          <ul class="global-nav__list">
            <li class="global-nav__item">
              <a href="Login">ログイン</a>
            </li>
           </ul>
          </nav>

        </header>
<body>
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">登録完了</h5>
		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s6 offset-s3">
				<div class="card grey lighten-5">
					<div class="card-content">
						<div class="row">
							<div class="input-field col s10 offset-s1">
                                <label>クレジット会社</label>
								<input type="text" value="${creditInfo.creditCompany}" readonly>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
                                <label>クレジット番号</label>
								<input type="text" value="${creditInfo.creditNum}" readonly>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
                                 <label>クレジット有効期限</label>
								<input type="text" value="${creditInfo.creditExDate}" readonly>
							</div>
						</div>
						<div class="row">
						<div class="input-field col s10 offset-s1">
                                <label>クレジット名　　　　</label>
							    <input type="text" value="${creditInfo.creditName}" readonly>
						</div>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">上記内容で登録しました。</p>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">
									<a href="Plat3" class="btn waves-effect waves-light  col s8 offset-s2">ホーム画面へ</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>