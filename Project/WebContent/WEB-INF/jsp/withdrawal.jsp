<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
   <head>
    <meta charset="utf-8"/>
    <title>Ring.jp</title>

    <link rel ="stylesheet" href="css/style1.css">
    <link rel="stylesheet" href="css/style.css"/>
</head>


    <header class="header">
    <div class="block"><h1 class="logo">
				<a href="Plat3"> Ring.jp </a>
			</h1></div>
    <div class="block"><a href="UserN">${userInfo.userName}</a></div>
    <div class="block"><ul class="example">
    	<li><a href="Favorite">お気に入り</a></li>
       <c:if test="${userInfo.loginId =='admin'}">
        <li><a href="Post">投稿</a></li>
        </c:if>
        <li><a href="Credit">クレジット変更</a></li>
        <li><a href="Logout">ログアウト</a></li></ul></div>
    </header>

<article class="login">

    <div class="container__headline">
    <section class="headline">
      <div class="headline__icon">
        <a class="link--nostyle" href="/">
          <img src="https://scontent-nrt1-1.xx.fbcdn.net/v/t1.0-9/1471088_803382306355162_1075974222_n.jpg?_nc_cat=105&_nc_sid=85a577&_nc_ohc=LnkdWAiSJxUAX8Lmmwb&_nc_ht=scontent-nrt1-1.xx&oh=2f7df61693cf0ea57406b3e3bca7c5e8&oe=5F4BCFCB" alt="Ring">

        </a>
      </div>
      <h1 class="headline__title fs-20">
        <span class="headline__title__text emphasis">退会のお手続き</span>
      </h1>
    </section>
  </div>


<form action="Withdrawal" method="Post">
  <div class="container__login__actions">
    <section class="login__actions">
      <a class="login__actions__item link--nostyle btn--primary" href="WithdrawalC">
        <span class="login__actions__item__text">退会</span>
      </a>
    </section>
  </div>
</form>
</article>


 <footer class="footer">
        <div class="footer__wrap">
          <nav class="footer__wrap__nav">
            <ul class="footer__wrap__nav__list">
                            <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle emphasis" href="Plat3">
                  <i class="icon icon-mr-10 icon--home"></i>
                  トップに戻る                </a>
              </li>
                                          <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle emphasis" href="Login">
                  ログイン                </a>
              </li>
                          </ul>
          </nav>
          <nav class="footer__wrap__nav">
            <ul class="footer__wrap__nav__list">
              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="TermService">
                  利用規約
                </a>
              </li>
              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="Privacy">
                  プライバシーポリシー                </a>
              </li>
              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="TransactLaw">
                  特定商取引法に基づく表記                </a>
              </li>
            </ul>
          </nav>
          <nav class="footer__wrap__nav">
            <ul class="footer__wrap__nav__list">
              <li class="footer__wrap__nav__list__item">
                              <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="Form" target="_blank">
                  お問い合わせ                              </a>
              </li>
                              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="Withdrawal">退会する</a>
                </li>
                          </ul>
          </nav>
          <div class="footer__wrap__copylight">
            <div class="footer__wrap__copylight__wrap">
              <span class="footer__wrap__copylight__wrap__text">© ring株式会社</span>
            </div>
          </div>
        </div>
      </footer>



</html>