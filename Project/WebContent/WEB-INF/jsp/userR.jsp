<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<<head>


<link rel ="stylesheet" href="css/style1.css">
    <link rel="stylesheet" href="css/style.css"/>
</head>

    <header class="header">
    <div class="block"><h1 class="logo">
				<a href="Plat3"> Ring.jp </a>
			</h1></div>
     <div class="block"><a href="UserN">${userInfo.userName}</a></div>
    <div class="block"><ul class="example">
    	<li><a href="Favorite">お気に入り</a></li>
    	<c:if test="${userInfo.loginId =='admin'}">
        <li><a href="Post">投稿</a></li>
        </c:if>
        <li><a href="Credit">クレジット変更</a></li>
        <li><a href="Logout">ログアウト</a></li></ul></div>
    </header>
<body>

	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">更新完了</h5>
		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<div class="row">
						<div class="input-field col s6">
                                <label>${userInfo.loginId}</label>
								<input type="hidden" value="" readonly>
							</div>
							<div class="input-field col s6">
                                 <label>名前</label>
								<input type="text" value="${userName}" readonly>
							</div>

						</div>
						<div class="row">
							<div class="input-field col s12">
                                <label>住所</label>
								<input type="text" value="${userAddress}" readonly>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">上記内容で更新しました</p>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">
									<a href="Plat3" class="btn waves-effect waves-light  col s4 offset-s4">マイページへ</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>