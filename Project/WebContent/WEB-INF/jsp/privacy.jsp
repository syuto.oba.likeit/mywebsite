<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8"/>


    <title>Ring.jp</title>
        <link rel="stylesheet" href="css/style1.css"/>
    <link rel="stylesheet" href="css/style.css"/>
</head>


    <header class="header">
      <h1 class="logo">
        <a href="plat">
          Ring.jp
        </a>
      </h1>
      <div class="header_profile">
        <ul>
                          <li><a href="Login">ログイン</a></li>
                  </ul>
      </div>


    </header>
        <main class="main">
      <div class="main__wrap">
                <div class="main__wrap__content">

<article class="guidelines">

    <div class="container__headline">
    <section class="headline">
      <div class="headline__icon">
        <a class="link--nostyle" href="/">
          <img src="https://scontent-nrt1-1.xx.fbcdn.net/v/t1.0-9/1471088_803382306355162_1075974222_n.jpg?_nc_cat=105&_nc_sid=85a577&_nc_ohc=LnkdWAiSJxUAX8Lmmwb&_nc_ht=scontent-nrt1-1.xx&oh=2f7df61693cf0ea57406b3e3bca7c5e8&oe=5F4BCFCB" alt="Ring">
        </a>
      </div>
      <h1 class="headline__title fs-20">
        <span class="headline__title__text emphasis">プライバシーポリシー</span>
      </h1>
    </section>
  </div>

  <div class="container__privacy">
    <section class="guidelines__privacy">

      <dl class="definition-list">
        <dt class="definition-list__term">
          個人情報の定義
        </dt>
        <dd class="definition-list__desc">
          <p>
            個人情報とは、利用者個人に関する情報であって、当該情報を構成する氏名、メールアドレス、生年月日その他の記述等により当該利用者を識別できるものとします。
          </p>
        </dd>
        <dt class="definition-list__term">
          個人情報の利用目的
        </dt>
        <dd class="definition-list__desc">
          <p>
            株式会社Ring(および株式会社Ringが提供するサービス全般)は、個人情報を以下の目的の範囲内のみで利用いたします。また、ご本人の同意がある場合及び個人情報保護法その他の個人情報に関する法令により認められる場合を除いては、この範囲を超えて個人情報を利用することはありません。
          </p>
          <ul>
            <li>
              <p>
                株式会社Ring(および株式会社Ringが提供するサービス全般)における利用者へのサービスの提供と個人認証
              </p>
            </li>
            <li>
              <p>
                商品広告を提供する企業等への会員サービスを目的とした情報提供
              </p>
            </li>
            <li>
              <p>
                株式会社Ring(および株式会社Ringが提供するサービス全般)によるサービスの向上等を目的としたアンケート、キャンペーンの実施
              </p>
            </li>
            <li>
              <p>
                株式会社Ring(および株式会社Ringが提供するサービス全般)からのメールによる各種情報の無料提供、お問い合わせへの返答
              </p>
            </li>
          </ul>
        </dd>
        <dt class="definition-list__term">
          個人情報の第三者提供
        </dt>
        <dd class="definition-list__desc">
          <p>
            株式会社Ring(および株式会社Ringが提供するサービス全般)は、以下の場合を除いては個人情報を第三者に提供をすることはありません。
          </p>
          <ul>
            <li>
              <p>
                本サービスのサービス向上の目的で個人情報を集計及び分析等する場合
              </p>
            </li>
            <li>
              <p>
                前号の集計及び分析等により得られたものを、個人を識別又は特定できない態様にて提携先等第三者に開示又は提供する場合
              </p>
            </li>
            <li>
              <p>
                本サービスに関わる部分の営業譲渡が行われ、譲渡先に対して法的に権利義務一切が引き継がれる場合
              </p>
            </li>
            <li>
              <p>
                個人情報の第三者への提供に当たりあらかじめ本人の同意を得ている場合
              </p>
            </li>
            <li>
              <p>
                法令に基づく場合
              </p>
            </li>
            <li>
              <p>
                人の生命、身体又は財産の保護のために必要がある場合であって、本人の同意を得ることが困難であるとき
              </p>
            </li>
            <li>
              <p>
                公衆衛生の向上またが児童の健全な育成の推進のために特に必要がある場合であって、本人の同意を得ることが困難であるとき
              </p>
            </li>
            <li>
              <p>
                国の機関若しくは地方公共団体又はその委託を受けた者が法令の定める事務を遂行することに対して協力する必要がある場合であって、本人の同意を得ることにより当該事務の遂行に支障を及ぼすおそれがあるとき
              </p>
            </li>
            <li>
              <p>
                利用目的の達成に必要な範囲内において、個人データの取扱いの全部又は一部を委託する場合
              </p>
            </li>
            <li>
              <p>
                商品広告を提供する企業等への会員サービスを目的とした情報提供
              </p>
            </li>
            <li>
              <p>
                その他法令により認められる場合
              </p>
            </li>
          </ul>
        </dd>
        <dt class="definition-list__term">
          個人情報の安全管理
        </dt>
        <dd class="definition-list__desc">
          <p>
            株式会社Ring(および株式会社Ringが提供するサービス全般)は、個人情報の漏えい、減失又は毀損の防止その他の安全管理のための措置を講じるとともに、株式会社Ring(および株式会社Ringが提供するサービス全般)の従業員に対する教育を行い、個人情報保護を徹底します。
          </p>
        </dd>
        <dt class="definition-list__term">
          個人情報の変更等
        </dt>
        <dd class="definition-list__desc">
          <p>
            利用者は、株式会社Ring(および株式会社Ringが提供するサービス全般)において登録した個人情報をいつでも変更・追加・削除することができます。
          </p>
        </dd>
        <dt class="definition-list__term">
          個人情報の利用目的の通知、開示、利用停止等の求め
        </dt>
        <dd class="definition-list__desc">
          <p>
            株式会社Ring(および株式会社Ringが提供するサービス全般)は、利用目的の通知及び個人情報の開示、利用停止又は消去、第三者への提供の停止について、下記の窓口にて受け付けております。ご本人からのお申出があった場合には、お申出頂いた方がご本人又はその代理人であることを確認のうえ、法令の定めに従い対応し、書面又はメールにて回答いたします。なお、お申出が法令の定める要件を充たさない場合、又は、個人情報保護法その他の法令により、開示等を拒絶することが認められる事由がある場合には、お申出に添えないことがございます。
          </p>
        </dd>
        <dt class="definition-list__term">
          お問い合わせ窓口
        </dt>
        <dd class="definition-list__desc">
          <p>
            開示等のお申出、ご意見、ご質問、苦情、その他個人情報の取り扱いに関するお問い合わせは、お問い合わせフォームよりご連絡ください。
          </p>
        </dd>
        <dt class="definition-list__term">
          免責
        </dt>
        <dd class="definition-list__desc">
          <p>
            以下の場合は、第三者による個人情報の取得に関し、株式会社Ring(および株式会社Ringが提供するサービス全般)は何らの責任を負いません。
          </p>
          <ul>
            <li>
              <p>
                利用者自らが株式会社Ring(および株式会社Ringが提供するサービス全般)上の機能または別の手段を用いて他の利用者に個人情報を明らかにした場合
              </p>
            </li>
            <li>
              <p>
                活動情報またはその他の利用者が入力した情報により、期せずして本人が特定できてしまった場合
              </p>
            </li>
            <li>
              <p>
                ご本人以外の利用者が個人を識別でき情報(ID、パスワード等)を入手した場合
              </p>
            </li>
          </ul>
        </dd>
        <dt class="definition-list__term">
          cookie(クッキー)の使用
        </dt>
        <dd class="definition-list__desc">
          <p>
            株式会社Ring(および株式会社Ringが提供するサービス全般)は、利用者の皆様によりよいサービスを提供するため、cookie(クッキー)を使用することがありますが、これにより個人を特定できる情報の収集を行えるものではなく、お客様のプライバシーを侵害することがございません。また、cookie(クッキー)の受け入れを希望されない場合は、ブラウザの設定で変更することができます。※cookie(クッキー)とは、サーバーコンピュータからのお客様のブラウザに送信され、お客様が使用しているコンピュータのハードディスクに蓄積される情報です。
          </p>
        </dd>
        <dt class="definition-list__term">
          アクセスログ等の記録、開示
        </dt>
        <dd class="definition-list__desc">
          <p>
            株式会社Ring(および株式会社Ringが提供するサービス全般)は、株式会社Ring(および株式会社Ringが提供するサービス全般)にメッセージ等の書き込みを行った登録ユーザーのIPアドレス、当該IPアドレスから株式会社Ring(および株式会社Ringが提供するサービス全般)に書き込みを行った日時等のアクセスログを記録することがあります。株式会社Ring(および株式会社Ringが提供するサービス全般)は、登録ユーザーが開示に同意した場合、犯罪捜査など法律手続の中で開示を要請された場合又は消費者センター、弁護士会等の公的機関から正当な理由に基づき照会を受けた場合の他、株式会社Ring(および株式会社Ringが提供するサービス全般)になされた書き込み等により損害を被ったと主張する第三者から開示を要請された場合において、当該書き込みを行った登録ユーザーの氏名、住所、電子メールアドレス、電話番号などの個人情報及びアクセスログを開示することがあります。
          </p>
        </dd>
        <dt class="definition-list__term">
          統計データの利用
        </dt>
        <dd class="definition-list__desc">
          <p>
            株式会社Ring(および株式会社Ringが提供するサービス全般)は、提供を受けた個人情報をもとに、個人を特定できない形式による統計データを作成し、当該データにつき何らの制限なく利用することができるものとします。
          </p>
        </dd>
        <dt class="definition-list__term">
          継続的改善
        </dt>
        <dd class="definition-list__desc">
          <p>
            株式会社Ring(および株式会社Ringが提供するサービス全般)は、個人情報の取り扱いに関する運用状況を適宜見直し、継続的な改善に努めるものとし、必要に応じて、プライバシーポリシーを変更することがあります。
          </p>
        </dd>
      </dl>
      <p>
        2017年12月1日 制定
      </p>
    </section>
  </div>

</article>        </div>
      </div>
    </main>

 <footer class="footer">
        <div class="footer__wrap">
          <nav class="footer__wrap__nav">
            <ul class="footer__wrap__nav__list">
                            <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle emphasis" href="Plat">
                  <i class="icon icon-mr-10 icon--home"></i>
                  トップに戻る                </a>
              </li>
                                          <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle emphasis" href="Login">
                  ログイン                </a>
              </li>
                          </ul>
          </nav>
          <nav class="footer__wrap__nav">
            <ul class="footer__wrap__nav__list">
              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="TermService">
                  利用規約
                </a>
              </li>
              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="Privacy">
                  プライバシーポリシー                </a>
              </li>
              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="TransactLaw">
                  特定商取引法に基づく表記                </a>
              </li>
            </ul>
          </nav>
          <nav class="footer__wrap__nav">
            <ul class="footer__wrap__nav__list">
              <li class="footer__wrap__nav__list__item">
                              <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="Form" target="_blank">
                  お問い合わせ                              </a>
              </li>
                              <li class="footer__wrap__nav__list__item">
                <a class="footer__wrap__nav__list__item__link box--row--center link--nostyle guideline" href="Withdrawal">退会する</a>
                </li>
                          </ul>
          </nav>
          <div class="footer__wrap__copylight">
            <div class="footer__wrap__copylight__wrap">
              <span class="footer__wrap__copylight__wrap__text">© ring株式会社</span>
            </div>
          </div>
        </div>
      </footer>



<ins class="adsbygoogle adsbygoogle-noablate" data-adsbygoogle-status="done" style="display: none !important;"><ins id="aswift_0_expand" style="display:inline-table;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:0px;background-color:transparent;"><ins id="aswift_0_anchor" style="display:block;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:0px;background-color:transparent;"><iframe id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;border:0;width:undefinedpx;height:undefinedpx;" sandbox="allow-forms allow-pointer-lock allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" frameborder="0" src="https://googleads.g.doubleclick.net/pagead/ads?client=ca-pub-9476476657694318&amp;output=html&amp;adk=1812271804&amp;adf=3025194257&amp;lmt=1596114657&amp;plat=1%3A32776%2C2%3A16809992%2C9%3A32776%2C10%3A32%2C11%3A32%2C16%3A8388608%2C17%3A32%2C24%3A32%2C25%3A32%2C30%3A1048576%2C32%3A32%2C40%3A32&amp;guci=2.2.0.0.2.2.0.0&amp;format=0x0&amp;url=https%3A%2F%2Fsalon.jp%2Fguidelines%2Fprivacy%2F&amp;ea=0&amp;flash=0&amp;pra=5&amp;wgl=1&amp;dt=1596114657443&amp;bpp=2&amp;bdt=123&amp;idt=21&amp;shv=r20200727&amp;cbv=r20190131&amp;ptt=9&amp;saldr=aa&amp;abxe=1&amp;nras=1&amp;correlator=1650393461705&amp;frm=20&amp;pv=2&amp;ga_vid=1567067410.1596083507&amp;ga_sid=1596114657&amp;ga_hid=297387856&amp;ga_fc=1&amp;iag=0&amp;icsg=537132704&amp;dssz=22&amp;mdo=0&amp;mso=0&amp;u_tz=540&amp;u_his=9&amp;u_java=0&amp;u_h=900&amp;u_w=1440&amp;u_ah=836&amp;u_aw=1440&amp;u_cd=30&amp;u_nplug=3&amp;u_nmime=4&amp;adx=-12245933&amp;ady=-12245933&amp;biw=1029&amp;bih=720&amp;scr_x=0&amp;scr_y=0&amp;eid=21066357&amp;oid=3&amp;pvsid=2196202752297094&amp;pem=718&amp;ref=https%3A%2F%2Fsalon.jp%2Fnishino&amp;rx=0&amp;eae=2&amp;fc=1920&amp;brdim=398%2C131%2C398%2C131%2C1440%2C23%2C1029%2C831%2C1029%2C720&amp;vis=1&amp;rsz=%7C%7Cs%7C&amp;abl=NS&amp;fu=8192&amp;bc=31&amp;ifi=0&amp;uci=a!0&amp;fsb=1&amp;dtd=30" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" data-google-container-id="a!0" data-load-complete="true"></iframe></ins></ins></ins><iframe id="google_osd_static_frame_1614606482464" name="google_osd_static_frame" style="display: none; width: 0px; height: 0px;"></iframe></body>